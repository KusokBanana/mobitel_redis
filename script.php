<?php
require_once 'vendor/autoload.php';

$options = array(
	'namespace' => 'Application_',
	'servers'   => array(
		array('host' => '127.0.0.1', 'port' => 6379),
		array('host' => '127.0.0.1', 'port' => 6380)
	)
);
$rediska = new Rediska($options);
$list = $rediska->getList('tasks');

if (isset($_GET['action'])) {

	$action = $_GET['action'];
	$url = $_SERVER['HTTP_REFERER'];
	if (strpos($url, '?') !== false) {
		$url = strstr($url, '?', true);
	}

	switch ($action) {
		case 'add':
			$value = $_GET['value'];
			$rediska->appendToList('tasks', $value);
			header('Location: ' . $url . '?add=' . $value);
			break;
		case 'delete':
			$value = $rediska->shiftFromList('tasks');
			header('Location: ' . $url . '?delete=' . $value);
	}

}



