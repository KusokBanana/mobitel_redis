<!doctype html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Rediska tasks</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">

    <script type="application/javascript">
        var myNewURI = refineURI();
        window.history.pushState("object or string", "Title", "/" + myNewURI );

        function refineURI() {
            var currURI= window.location.href; //get current address
            var afterDomain= currURI.substring(currURI.lastIndexOf('/') + 1);
            return afterDomain.split("?")[0];
        }
    </script>

</head>
<body>
<?php require_once 'script.php'?>
<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
	<li class="nav-item">
		<a class="nav-link <?= isset($_GET['delete']) ? '' : 'active' ?>" data-toggle="tab" href="#tasksAdd" role="tab">
            New task
        </a>
	</li>
	<li class="nav-item">
		<a class="nav-link <?= isset($_GET['delete']) ? 'active' : '' ?>" data-toggle="tab" href="#tasksDelete" role="tab">
            Tasks
        </a>
	</li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
	<div class="tab-pane <?= isset($_GET['delete']) ? '' : 'active' ?>" id="tasksAdd" role="tabpanel">

        <?php if (isset($_GET['add']) && $_GET['add']): ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                New task <strong>"<?= $_GET['add'] ?>"</strong> was successfully added
            </div>
        <?php endif; ?>
        <br>
        <form action="script.php" method="get" id="addForm">
            <div class="form-group mx-sm-3">
                <label for="taskName">Task</label>
                <input class="form-control" id="taskName" name="value">
                <input type="hidden" name="action" value="add">
            </div>
            <div class="mx-sm-3">
                <button type="submit" class="btn btn-primary" id="taskAddBtn">Add</button>
            </div>
        </form>
	</div>
	<div class="tab-pane <?= isset($_GET['delete']) ? 'active' : '' ?>" id="tasksDelete" role="tabpanel">

		<?php if (isset($_GET['delete']) && $_GET['delete']): ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                Task <strong>"<?= $_GET['delete'] ?>"</strong> was successfully removed
            </div>
		<?php endif; ?>
        <br>
        <form action="script.php" method="get" id="deleteForm">
            <input type="hidden" name="action" value="delete">
            <div class="mx-sm-3">
                <button type="submit" class="btn btn-danger" id="taskAddBtn">Remove last</button>
            </div>
        </form>
        <br>
        <div class="mx-sm-3">
            <ul class="list-group">
		        <?php
		        if (!empty($list)) {
			        foreach ($list as $value) {
				        echo '<li class="list-group-item">' . $value . '</li>';
			        }
		        }
		        ?>
            </ul>
        </div>
	</div>
</div>

<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>

</body>
</html>